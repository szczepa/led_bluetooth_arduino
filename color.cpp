#include "color.h"


Color::Color()
{
 _r = Serial.parseInt();
 _g = Serial.parseInt();
 _b = Serial.parseInt();
}
  
Color::Color(uint8_t r, uint8_t g, uint8_t b) : _r(r), _g(g), _b(b) {}

Color::Color(const Color &c)
{
	_r = c.getRed();
	_g = c.getGreen();
	_b = c.getBlue();
}

uint8_t Color::getRed() const
{
  	return _r;
}

uint8_t Color::getGreen() const
{
	return _g;
}

uint8_t Color::getBlue() const
{
 	return _b;
}

void Color::modifyColor(uint8_t r, uint8_t g, uint8_t b)
{
	_r = r;
	_g = g;
	_b = b;
}

void Color::modifyColor(const Color& c)
{
	_r = c.getRed();
	_g = c.getGreen();
	_b = c.getBlue();
}

/*void operator=(const Color & c)
{
	if(c != this)
	{
		modifyColor(c);
	}
}*/

void Color::InRed()
{
	_r++;
}

void Color::DeRed()
{
	_r--;
}

void Color::InGreen()
{
	_g++;
}

void Color::DeGreen()
{
	_g--;
}

void Color::InBlue()
{
	_b++;
}

void Color::DeBlue()
{
	_b--;
}