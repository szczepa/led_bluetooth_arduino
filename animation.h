#ifndef _animation_h
#define _animation_h

#include <Arduino.h>
#include "defines.h"
#include "color.h"


class Animation
{
private:
	uint8_t  _r_steps[MAX_ANIMATION_SIZE];
	uint8_t  _g_steps[MAX_ANIMATION_SIZE];
	uint8_t  _b_steps[MAX_ANIMATION_SIZE];

	uint8_t  _current_step;

	uint8_t  _size_iterator; 
	uint8_t  _size; 

	uint8_t  _iterator;

	void IsFull();

	bool     _full;

public:

	Animation();
	explicit Animation(const uint8_t);


	void AddColor(const Color &);
	void AddColor(const uint8_t, const uint8_t, const uint8_t);

	uint8_t getRed();
	uint8_t getGreen();
	uint8_t getBlue();
	uint8_t size();
	uint8_t step();

};

#endif