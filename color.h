#ifndef _color_h
#define _color_h

#include <Arduino.h>

class Color
{
private:
  uint8_t _r;
  uint8_t _g;
  uint8_t _b; 

public:
  explicit Color();

  explicit Color(uint8_t r, uint8_t g, uint8_t b);

           Color(const Color &);

  uint8_t getRed()   const;
  uint8_t getGreen() const;
  uint8_t getBlue()  const;

  void modifyColor(uint8_t, uint8_t, uint8_t);
  void modifyColor(const Color&);

  //void operator=(const Color&);

  void InRed();
  void DeRed();

  void InGreen();
  void DeGreen();

  void InBlue();
  void DeBlue();


};


#endif