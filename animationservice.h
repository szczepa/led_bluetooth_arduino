#ifndef _animation_serivce_h
#define _animation_serivce_h

#include <Arduino.h>

#include "animation.h"
#include "color.h"

class AnimationService
{
private:
	Animation * _animation;
	bool 		_start;
	int 		_time;
	Color       _current_color;
  

public:
	AnimationService();

	void  startAnimation();
	void  stopAnimation();
	void  setCurrentColor(Color &);
	Color animate();  
	bool  isStarted();

	void  changeAnimation(Animation *);
	void  changeTime(int);

};


#endif