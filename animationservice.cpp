#include "animationservice.h"


AnimationService::AnimationService() : _start(false), _time(10)
{}


void AnimationService::startAnimation()
{
	_start = true;
}

void AnimationService::stopAnimation()
{
	_start = false;
}

void AnimationService::setCurrentColor(Color & c)
{
	_current_color = c;
}

Color AnimationService::animate()
{
	if(_start)
	{
      delay(_time);
      {  
        if(_current_color.getRed() < _animation->getRed())
        {_current_color.InRed();}
        else if(_current_color.getRed() > _animation->getRed())
        {_current_color.DeRed();}

        if(_current_color.getGreen() < _animation->getGreen())
        {_current_color.InGreen();}
        else if(_current_color.getGreen() > _animation->getGreen())
        {_current_color.DeGreen();}

        if(_current_color.getBlue() < _animation->getBlue())
        {_current_color.InBlue();}
        else if(_current_color.getBlue() > _animation->getBlue())
        {_current_color.DeBlue();}

        if(_current_color.getRed() == _animation->getRed() 
        	&& _current_color.getGreen() == _animation->getGreen() 
        	&& _current_color.getBlue() == _animation->getBlue()
        )
        {
        	_animation->step();
        }

      }
    }
     return _current_color; 
}

bool AnimationService::isStarted()
{
	return _start;
}

void AnimationService::changeAnimation(Animation * a)
{
	_animation = a;
}

void AnimationService::changeTime(int t)
{
	_time = t;
}






