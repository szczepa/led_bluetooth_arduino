#include "animation.h"

Animation::Animation()
{
	_size = MAX_ANIMATION_SIZE; 
	_full = false;
	_iterator = 0;
	_size_iterator = 0; 
}

Animation::Animation(uint8_t size) : _size_iterator(0), _size(size), _iterator(0), _full(false)
{}

void Animation::AddColor(const Color&  c)
{
	return AddColor(c.getRed(), c.getGreen(), c.getBlue());
}

void Animation::AddColor(const uint8_t r, const uint8_t g, const uint8_t b  )
{
	IsFull();

	if(!_full)
	{
		_r_steps[_size_iterator] = r;
		_g_steps[_size_iterator] = g;
		_b_steps[_size_iterator] = b;
		
		++_size_iterator;
	}
	else 
	{
		_size_iterator = 0;
		_full = false;
		_r_steps[_size_iterator] = r;
		_g_steps[_size_iterator] = g;
		_b_steps[_size_iterator] = b;
	}
}

void Animation::IsFull()
{ 
	if (_size_iterator  == _size)
		{
			_full = true;
		}
}

uint8_t Animation::step()
{
	_iterator++;
	if(_iterator == _size)
	{
		_iterator = 0;
	}

	return _iterator;
}


uint8_t Animation::getRed()
{
	return _r_steps[_iterator];
}

uint8_t Animation::getGreen()
{
	return _g_steps[_iterator];
}

uint8_t Animation::getBlue()
{
	return _b_steps[_iterator];
}
uint8_t Animation::size()
{
	return _size;
}