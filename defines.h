#ifndef _defines
#define _defines

/*
	PIN definitons 

*/
#define BLUE_PIN 11
#define RED_PIN  10
#define GREEN_PIN 9

/*
	MODE definitions 
	Used in switch, are beginning of message
*/
#define MODE_NULL         -1
#define MODE_STATIC_COLOR 1
#define MODE_EPROM_WRITE  2
#define MODE_START_ANIMATION 3
#define MODE_STOP_ANIMATION 4 
#define MODE_CHANGE_ANIMATION 5
#define MODE_DYNAMIC_COLOR 6
#define MODE_POLLING 7
#define MODE_CHANGE_TIME 8

#define MODE_TEST_RED 99

#define MAX_ANIMATION_SIZE 10

#define POLLING_ANS "HI"

#endif