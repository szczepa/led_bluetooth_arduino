#include "defines.h"

#include "color.h"
#include "animation.h"
#include "animationservice.h"

#define WORK_STATUS  "Przejscie na wskazniki"

/* TO DO
  - dodać klasę paska 
  - AS zmienić delay w animate 
  - MODE_CHANGE_TIME sprawdzanie czasu
  - dynamic color nie działa - naprawić
  -
*/



int r = 0,g = 0,b = 0;
int Mode = 0;

Color currentColor(0,0,0);
Color tempColor(0,0,0);

Animation * a1;
Animation * globalAnimation;

AnimationService * AS;
AnimationService * AS_global;


void Pisz(uint8_t _r, uint8_t _g, uint8_t _b)
{
  analogWrite(RED_PIN,_r);
  analogWrite(GREEN_PIN,_g);
  analogWrite(BLUE_PIN,_b);
}

void Pisz(const Color &c)
{
  Pisz(c.getRed(),c.getGreen(),c.getBlue());
}

void setup()
{
  pinMode(BLUE_PIN, OUTPUT);
  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(13,OUTPUT);
  
  Serial.begin(9600);
  Serial.println(WORK_STATUS);
  
  AS = new AnimationService;
  AS_global = new AnimationService;
  a1 = new Animation(5);
  globalAnimation = new Animation(2);

  analogWrite(RED_PIN, 15);
  analogWrite(GREEN_PIN, 15);
  analogWrite(BLUE_PIN, 15);

  delay(200);

  analogWrite(RED_PIN, 0);
  analogWrite(GREEN_PIN, 0);
  analogWrite(BLUE_PIN, 0);  

  a1->AddColor(0,0,255);
  a1->AddColor(70,0,180);
  a1->AddColor(0,70,255);
  a1->AddColor(90,80,179);
  a1->AddColor(50,100,150);

  globalAnimation->AddColor(0,0,0);

  AS->changeAnimation(a1);
  AS_global->changeAnimation(globalAnimation);
  
}



void loop()
{

  if(Serial.available() > 0 )
   {

    Mode = Serial.parseInt();

    Serial.print("MODE  = "); Serial.println(Mode);

    switch (Mode) 
    {

        case MODE_NULL:       
          currentColor.modifyColor(0,0,0);
          Serial.flush();
          break;

        case MODE_STATIC_COLOR:
          AS_global->stopAnimation();
          r = Serial.parseInt();
          g = Serial.parseInt();
          b = Serial.parseInt(); 
          currentColor.modifyColor(r,g,b);  
          Serial.flush(); 
          break;

        case MODE_EPROM_WRITE:
          break;

        case MODE_START_ANIMATION:
          AS->startAnimation();
          Serial.println("Animation started");
          Serial.flush();
          break;

        case MODE_STOP_ANIMATION:
          AS->stopAnimation();
          Serial.println("Animation ended");
          Serial.flush();
          break;

        case MODE_CHANGE_ANIMATION:
          break;

        case MODE_DYNAMIC_COLOR:
          r = Serial.parseInt();
          g = Serial.parseInt();
          b = Serial.parseInt();  
          globalAnimation->AddColor(r,g,b);
          Serial.flush();
          break;

        case MODE_POLLING:
           Serial.println(POLLING_ANS);
           Serial.flush();
           break;

        case MODE_TEST_RED:
            currentColor.modifyColor(255,0,0);
            Serial.flush();
            break;

        case MODE_CHANGE_TIME:
            AS->changeTime(Serial.parseInt());
            Serial.flush();
            break;

        default:
          Serial.println("Wrong mode specifier.");


        break;
          
    }
   }
        

  if(AS->isStarted())
  {
    AS_global->stopAnimation();
    currentColor = AS->animate(); 
  }

  if(AS_global->isStarted())
  {
    AS->stopAnimation();
    currentColor = AS_global->animate();
  }


  Pisz(currentColor);    


}